from django.core.validators import MinValueValidator
from django.db import models

# Create your models here.
class User(models.Model):
  username = models.CharField(max_length=20, primary_key=True)
  email = models.TextField(max_length=20)
  password = models.TextField(max_length=20)

class Gift(models.Model):
  name = models.CharField(max_length=20)