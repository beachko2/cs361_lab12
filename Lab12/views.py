from django.shortcuts import render
from django.views import View
from Lab12.models import User

from Lab12.models import User
# Create your views here.


# Create your views here.


# Create your views here.
class Home(View):

    # No additional fields.

    def get(self, request):
        return render(request, 'login.html')


    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]

        print(username + " " + password)

        return render(request, 'registration.html')

class Registration(View):

    errorMessage = ""


    def get(self, request):
        return render(request, 'registration.html')


    def post(self, request):
        email = request.POST["email"]
        username = request.POST["username"]
        password = request.POST["password"]

        if Registration.doesUsernameAlreadyExist(username):
            return render(request, 'registration.html', {"errorMessage": Registration.errorMessage})
        else: #username does not already exist
            Registration.createAccount(username, email, password)
            return render(request, 'success.html')


    # Helper functions below:


    @staticmethod
    def doesUsernameAlreadyExist(username):
        if User.objects.filter(username = username).count() > 0:
            Registration.errorMessage = "Error, entered username already exists!"
            return True
        return False

    @staticmethod
    def createAccount(username, emailAddress, password):
        User.objects.create(username = username,
                            email = emailAddress,
                            password = password)

# End of class Registration.

class Success(view):
    def get(self, request):
        return render(request, 'success..html')


class User(View):

    def get(self, request):
        return render(request, 'userlist.html')

    def post(self, request):
        queryset = User.objects.all()

# End of class UserList.

class Gift(View):

    def get(self, request):
        return render(request, 'giftlist.html')

    def post(self, request):
        queryset = Gift.objects.all()

# End of class GiftList.
